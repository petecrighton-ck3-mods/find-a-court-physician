action_no_court_physician = {
    check_create_action = {
        if = {
            limit = {
                any_courtier = {
                    NOT = {
                        has_relation_court_physician = yes
                    }
                }
            }
            try_create_important_action = {
                important_action_type = action_no_court_physician
                actor = root
            }
        }
    }

    effect = {
		root = {
            open_view_data = {
                view = court_window
            }
        }
    }
}
